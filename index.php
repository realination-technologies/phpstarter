<?php
/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 6/20/2016
 * Time: 5:36 PM
 */

session_start();

include_once "server/config/routes.php";

include_once "server/config/Database.php";


foreach(glob('server/system/*.php') as $file):
    include $file;
endforeach;

foreach(glob('server/model/*.php') as $file):
    include $file;
endforeach;

foreach(glob('server/service/*.php') as $file):
    include $file;
endforeach;





if(isset($_SERVER['PATH_INFO'])) {
    $method = strtolower($_SERVER['REQUEST_METHOD']);
    $path = strtolower($_SERVER['PATH_INFO']);
    if (isset($route[strtolower($path)][strtolower($method)])) {
        $uri = $route[strtolower($path)][strtolower($method)];
        openController($uri);
    }else{
        openController($route['error']);
    }
}else{
    openController($route['default']);
}



function openController($uri){
        $request = explode("/", $uri);
        $controllerName = $request[0];
        $functionName = $request[1];
        include_once "controller/" . $controllerName . ".php";
        $controller = new $controllerName();

        $controller->$functionName();

}