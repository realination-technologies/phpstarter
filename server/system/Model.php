<?php

/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 7/15/2016
 * Time: 4:00 PM
 */
abstract class Model{

    protected $_table_name = '';
    protected $conn;

    public function __construct(){
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    public function findAll(){
        $sql = "SELECT * FROM `".$this->_table_name."`";
        $query = $this->conn->query($sql);
        return $query->fetchAll();
    }

    public function findById($id){
        $sql = "SELECT * FROM `".$this->_table_name."` WHERE `id` = '$id'";
        $query = $this->conn->query($sql);
        return $query->fetch();
    }

    public function findWithConditions($conditions,$single = false, $fields = "*"){
        $where = " WHERE ";
        $i = 0;
        foreach($conditions as $condition) {
            $where .= "`".$condition->getField()."` ".$condition->getCondition()." '".$condition->getValue()."'";
            if($i < count($conditions)-1){
                $where .= " AND ";
            }
            $i++;
        }

        $sql = "SELECT $fields FROM `".$this->_table_name."`".$where;
        $query = $this->conn->query($sql);

        if($single){
            return $query->fetch();
        }else{
            return $query->fetchAll();
        }
    }


    public function insert($fields,$values){
        $sql = "INSERT INTO `".$this->_table_name."` (".implode(",",$fields).")VALUES(".$this->generateFillers($fields).")";
        $query = $this->conn->prepare($sql);
        $query->execute($values);
    }


    public function generateFillers($fld){
        $fillers = array();
       foreach($fld as $f){
           array_push($fillers,"?");
       }
        return implode(",",$fillers);
    }

    public function update($fields,$values,$conditions){
        $where = " WHERE ";
        $i = 0;
        foreach($conditions as $condition) {
            $where .= "`".$condition->getField()."` ".$condition->getCondition()." '".$condition->getValue()."'";
            if($i < count($conditions)-1){
                $where .= " AND ";
            }
            $i++;
        }

        $sql = "UPDATE ".$this->_table_name." SET ".implode(" , ",$fields)." ".$where;
        $query = $this->conn->prepare($sql);
        $query->execute($values);

    }
}