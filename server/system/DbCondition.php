<?php

/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 7/15/2016
 * Time: 5:00 PM
 */
class DbCondition{

    private $field;
    private $condition;
    private $value;

    /**
     * DbCondition constructor.
     * @param $field
     * @param $condition
     * @param $value
     */
    public function __construct($field, $condition, $value)
    {
        $this->field = $field;
        $this->condition = $condition;
        $this->value = $value;
    }


    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }



}