<?php

/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 7/15/2016
 * Time: 3:18 PM
 */
class AuthenticationService{


    /**
     * AuthenticationService constructor.
     */
    public function __construct(){
        $this->accounts = new Accounts();
    }

    function generateSalt(){
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0987654321'.'!)@(#*$&%^<>?{}|:][;,.');
        $rand = '';
        foreach(array_rand($seed, 10) as $k){
            $rand .= $seed[$k];
        }
        return $rand;
    }

    function encryptString($password,$salt){
        return base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $salt ), $password, MCRYPT_MODE_CBC, md5( md5( $salt ) ) ) );
    }


    function decryptString($password,$salt){
        return rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $salt ), base64_decode( $password ), MCRYPT_MODE_CBC, md5( md5( $salt ) ) ), "\0");
    }


    function authenticate($nral,$password){
        $conditions = array();
        array_push($conditions, new DbCondition("nralcode","LIKE",$nral));
        $account = $this->accounts->findWithConditions($conditions,true);
        if(count($account) > 0) {
            if($this->encryptString($password,$account['salt']) === $account['password']){
                $this->setSession($nral, $account['level'],$account['id'],$account['name']);
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }



    function setSession($nral,$lvl,$id,$name){
        $_SESSION['nral'] = $nral;
        $_SESSION['level'] = $lvl;
        $_SESSION['id'] = $id;
        $_SESSION['name'] = $name;
    }

    function isAuthenticated(){
        if(isset($_SESSION['id'])){
            return true;
        }else{
            return false;
        }
    }

    function isAdmin(){
        if($_SESSION['level'] == "0"){
            return true;
        }else{
            return false;
        }
    }


}