<?php

/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 7/19/2016
 * Time: 4:10 PM
 */
class DataService{


    /**
     * DataService constructor.
     */
    public function __construct(){
        $this->linkup = new Linkup();
    }

    function insertData($fields,$values){
        $this->linkup->insert($fields,$values);
    }

    function updateData($fields,$values,$prgCode,$nralCode,$fld1){
        $conditions = array();
        $condition1 = new DbCondition("Prgcode","LIKE",trim($prgCode));
        $condition2 = new DbCondition("NRALcode","LIKE",trim($nralCode));
        $condition3 = new DbCondition("fld1","LIKE",trim($fld1));

        array_push($conditions,$condition1);
        array_push($conditions,$condition2);
        array_push($conditions,$condition3);

        $this->linkup->update($fields,$values,$conditions);
    }

    function recordExists($prgCode,$nralCode,$fld1){
       $conditions = array();
        $condition1 = new DbCondition("Prgcode","LIKE",trim($prgCode));
        $condition2 = new DbCondition("NRALcode","LIKE",trim($nralCode));
        $condition3 = new DbCondition("fld1","LIKE",trim($fld1));

       array_push($conditions,$condition1);
        array_push($conditions,$condition2);
        array_push($conditions,$condition3);

        $recs = $this->linkup->findWithConditions($conditions);

        if(count($recs) > 0){
            return true;
        }else{
            return false;
        }
    }

}