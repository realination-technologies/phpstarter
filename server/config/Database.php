<?php

/**
 * Created by PhpStorm.
 * User: Troll173
 * Date: 7/15/2016
 * Time: 3:53 PM
 */
class Database{

    protected $dsn = 'mysql:dbname=iphp;host=localhost';
    protected $user = 'root';
    protected $password = '';

    public function getConnection(){
        try {
            $conn = new PDO($this->dsn, $this->user, $this->password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
            return null;
        }
    }


}